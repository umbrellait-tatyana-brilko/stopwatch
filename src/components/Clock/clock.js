import React, { Component } from "react";

import ButtonStopwatch from "../Buttons/buttonStopwatch";

import "../app.css";

export default class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seconds: 0,
      minuts: 0,
      isToggleOn: true,
    };

    this.handleClick = this.handleClick.bind(this);
  }

  tick() {
    var seconds = this.state.seconds + 1;
    var minuts = this.state.minuts;
    if (seconds === 10) {
      minuts = Math.floor(seconds / 10);
      minuts = this.state.minuts + 1;
      seconds = seconds % 10;
    }

    this.setState({
      seconds: seconds,
      minuts: minuts,
    });
  }

  timerInterval() {
    this.timer = setInterval(() => {
      this.tick();
    }, 1000);
  }

  handleClick() {
    this.setState((state) => ({
      isToggleOn: !state.isToggleOn,
    }));
    if (this.state.isToggleOn) {
      this.timerInterval();
    } else {
      clearInterval(this.timer);
    }
  }

  buttonDrop = () => {
    clearInterval(this.timer);
    this.timeZero = this.setState({
      seconds: 0,
      minuts: 0,
      isToggleOn: true,
    });
    return this.timeZero;
  };

  render() {
    return (
      <div>
        <div className="stopwatch">
          {this.state.minuts} : {this.state.seconds}
        </div>
        <ButtonStopwatch
          className={this.state.isToggleOn ? "Start" : "Stop"}
          onClick={this.handleClick}
        >
          {this.state.isToggleOn ? "START" : "STOP"}
        </ButtonStopwatch>
        <ButtonStopwatch className={"Drop"} onClick={this.buttonDrop}>
          DROP
        </ButtonStopwatch>
      </div>
    );
  }
}
