import React from "react";

import styles from "./Buttons.module.css";
import classNames from "classname";

const ButtonStopwatch = (props) => {
  return (
    <button
      className={classNames(props.className, styles.button)}
      onClick={props.onClick}
    >
      {props.children}
    </button>
  );
};
export default ButtonStopwatch;
