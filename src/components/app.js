import React from "react";

import Clock from "./Clock/clock";

import "./app.css";

const App = () => {
  return (
    <div className="app">
      <h1> Stopwatch </h1>
      <Clock />
    </div>
  );
};
export default App;
